///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file dog.cpp
/// @version 1.0
///
/// Exports data about all dogs
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
/// @date   17_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "dog.hpp"

using namespace std;

namespace animalfarm {

   /// note to self this is the constructor 
Dog::Dog( string newName, enum Color newColor, enum Gender newGender ) {
	gender = newGender;         /// This is a has-a relationship
	species = "Canis lupus";    /// This is a is-a relationship
	hairColor = newColor;       /// A has-a relationship, so it comes through the constructor
	gestationPeriod = 64;       /// An is-a relationship, hardcode...  All dogs have the same gestation period.
	name = newName;             /// A has-a relationship.
}

  /// note to self this defines the speak method
const string Dog::speak() {
	return string( "Woof" );
}


/// Print our Dog and name first... then print whatever information Mammal holds.
void Dog::printInfo() {
	cout << "Dog Name = [" << name << "]" << endl;
	Mammal::printInfo();
}

} // namespace animalfarm
