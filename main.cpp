///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
/// @date   17_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <list>
#include <cassert>

#include "animalfactory.hpp"
#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main() {
   
   cout << "Welcome to Animal Farm 3" << endl;

   // 25 animals in an array
   array<Animal*, 30> animalArray;
   animalArray.fill( NULL );
   for( int i = 0 ; i < 25 ; i++ ) {
      animalArray[i] = AnimalFactory::getRandomAnimal();
	}

   cout << endl << "Array of Animals: "      << endl;
   cout <<         "   Is it empty: "        << boolalpha << animalArray.empty() << endl;
   cout <<         "   Number of elements: " << animalArray.size() << endl;
   cout <<         "   Max size: "           << animalArray.max_size() << endl;

   for ( Animal* pAnimal : animalArray ) {   // range-based for loop
      if ( pAnimal == NULL ) break;          // check if the end of the array elements has been reached
      cout << pAnimal->speak() << endl;
   }
   
   for ( Animal* pAnimal : animalArray ) { 
      if ( pAnimal != NULL ){
         delete pAnimal;
      }
   }
   cout << endl;

   // 25 Animals in a list
   list<Animal*> animalList;
   for( int i = 0 ; i < 25 ; i++ ) {
      animalList.push_front( AnimalFactory::getRandomAnimal() );
	}

   cout << endl << "List of Animals: "      << endl;
   cout <<         "   Is it empty: "        << boolalpha << animalList.empty() << endl;
   cout <<         "   Number of elements: " << animalList.size() << endl;
   cout <<         "   Max size: "           << animalList.max_size() << endl;

   for ( Animal* pAnimal : animalList ) {   // range-based for loop
      if ( pAnimal == NULL ) break;          // check if the end of the array elements has been reached
      cout << pAnimal->speak() << endl;
   }
   
   for ( Animal* pAnimal : animalList )   delete pAnimal;
   for( int i = 0 ; i < 25 ; i++ )        animalList.pop_front();
   cout << endl;
	
   return 0;
}
