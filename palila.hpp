///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file palila.hpp
/// @version 1.0
///
/// Exports data about all palila
///
/// @author Emily_Pham <emilyn3@hawaii.edu>
/// @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
/// @date   17_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "bird.hpp"

using namespace std;


namespace animalfarm {

class Palila : public Bird {
public:
   string whereFound;
   
   Palila( string newWhereFound, enum Color newColor, enum Gender newGender );
	
	void printInfo();
};

} // namespace animalfarm
