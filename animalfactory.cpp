///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animalfactory.cpp
/// @version 1.0
///
/// Generates random animals
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
/// @date   11_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "animalfactory.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
namespace animalfarm {

Animal* AnimalFactory::getRandomAnimal() {
   Animal* newAnimal = NULL;
   
   random_device  rdSeed;
   mt19937        mtRNG ( rdSeed() );
   uniform_int_distribution<> randomNumber{0,5};

   switch( randomNumber( mtRNG ) ) {
         case 0:  newAnimal = new Cat     ( Animal::getRandomName(),                           Animal::getRandomColor(), Animal::getRandomGender() ); break;
         case 1:  newAnimal = new Dog     ( Animal::getRandomName(),                           Animal::getRandomColor(), Animal::getRandomGender() ); break;
         case 2:  newAnimal = new Nunu    ( Animal::getRandomBool(),                           RED,                      Animal::getRandomGender() ); break;
         case 3:  newAnimal = new Aku     ( Animal::getRandomWeight( MAX_WEIGHT, MIN_WEIGHT ), SILVER,                   Animal::getRandomGender() ); break;
         case 4:  newAnimal = new Palila  ( Animal::getRandomName(),                           YELLOW,                   Animal::getRandomGender() ); break;
         case 5:  newAnimal = new Nene    ( Animal::getRandomName(),                           BROWN,                    Animal::getRandomGender() ); break;
   } // end switch

   return newAnimal;
}

} // namespace animalfarm
