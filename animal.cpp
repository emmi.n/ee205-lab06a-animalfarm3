///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
/// @date   17_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include<random>

#include "animal.hpp"

using namespace std;

namespace animalfarm {

Animal::Animal() {
   cout << "." ;
}

Animal::~Animal() {
   cout << "x" ;
}


void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male");      break;
      case FEMALE:  return string("Female");    break;
      case UNKNOWN: return string("Unknown");   break;
   }

   return string("Really, really Unknown");
}
	

string Animal::colorName (enum Color color) {
/// Decode the enum Color into strings for printf()
   switch (color) {
      case BLACK:    return string("Black");       break;
      case WHITE:    return string("White");       break;
      case BROWN:    return string("Brown");       break;
      case SILVER:   return string("Silver");      break;
      case RED:      return string("Red");         break;
      case YELLOW:   return string("Yellow");      break;
      case GREEN:    return string("Green");       break;
      case BLUE:     return string("Blue");        break;
   }

   return string("Unknown");
}


const Gender Animal::getRandomGender() {
   random_device  rdSeed;
   mt19937        mtRNG ( rdSeed() );
   unsigned int   randomGender;

   randomGender = mtRNG() % 2;

   switch ( randomGender ) {
      case 0:  return MALE;         break;
      case 1:  return FEMALE;       break;
   }
   return UNKNOWN;
}

const Color Animal::getRandomColor() {
   random_device  rdSeed;
   mt19937        mtRNG ( rdSeed() );
   uniform_int_distribution<> randomNumber{0,7};

   switch ( randomNumber( mtRNG ) ) {
      case 0:  return BLACK;       break;
      case 1:  return WHITE;       break;
      case 2:  return BROWN;       break;
      case 3:  return SILVER;      break;
      case 4:  return RED;         break;
      case 5:  return YELLOW;      break;
      case 6:  return GREEN;       break;
      case 7:  return BLUE;        break;
   }
   return BLACK;
}

const bool Animal::getRandomBool(){
   random_device           rdSeed;
   mt19937                 mtRNG ( rdSeed() );
   bernoulli_distribution  randBool(0.30); // 30% of the time it will output true, else false

   return randBool( mtRNG );
}

const float Animal::getRandomWeight( const float from, const float to ){
   random_device           rdSeed;
   mt19937                 mtRNG ( rdSeed() );
   normal_distribution<>   randomWeight{ from , to };

   return randomWeight( mtRNG );  
}

const string Animal::getRandomName() {
   random_device  rdSeed;
   mt19937        mtRNG ( rdSeed() );
   uniform_int_distribution<> randomLength{3,10};
   uniform_int_distribution<> randomUppercaseLetterFromAtoZ{65,90}; // range corresponds to ASCII values
   uniform_int_distribution<> randomLowercaseLetterFrom_aTOz{97,122}; // range coressponds to ASCII values

   string randomName;
   int length = randomLength( mtRNG );
  
   char firstLetter = (char)randomUppercaseLetterFromAtoZ( mtRNG ); 
   randomName.append ( &firstLetter,1 );

   char nextLetter;
   for ( int i = 1; i < length; i++) {
         nextLetter = (char)randomLowercaseLetterFrom_aTOz( mtRNG );
         randomName.append ( &nextLetter,1 );
   } //end for
   return randomName;
}
} // namespace animalfarm
